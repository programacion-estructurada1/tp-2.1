#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
	
	/* Punto 1 - Ingresar dos valores e imprimir si son iguales */
	
	int n1, n2;
	
	printf("Ingrese el primer valor: \n");
	scanf("%d", &n1);
	printf("Ingrese el segundo valor: \n");
	scanf("%d", &n2);
	
	if (n1 == n2){
		printf("Son iguales.\n");
	}else{
		printf("Son distintos.\n");
	}
	
	return 0;
	
}
